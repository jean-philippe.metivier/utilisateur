<?php

use Doctrine\Common\Persistence\Mapping\Driver\MappingDriverChain;
use Doctrine\ORM\Mapping\Driver\XmlDriver;
use UnicaenUtilisateur\ORM\Event\Listeners\HistoriqueListener;
use UnicaenUtilisateur\ORM\Event\Listeners\HistoriqueListenerFactory;
use UnicaenUtilisateur\View\Helper\RoleViewHelper;
use UnicaenUtilisateur\View\Helper\UserConnection;
use UnicaenUtilisateur\View\Helper\UserConnectionFactory;
use UnicaenUtilisateur\View\Helper\UserCurrent;
use UnicaenUtilisateur\View\Helper\UserCurrentFactory;
use UnicaenUtilisateur\View\Helper\UserInfo;
use UnicaenUtilisateur\View\Helper\UserInfoFactory;
use UnicaenUtilisateur\View\Helper\UserProfile;
use UnicaenUtilisateur\View\Helper\UserProfileFactory;
use UnicaenUtilisateur\View\Helper\UserProfileSelect;
use UnicaenUtilisateur\View\Helper\UserProfileSelectFactory;
use UnicaenUtilisateur\View\Helper\UserProfileSelectRadioItem;
use UnicaenUtilisateur\View\Helper\UserProfileSelectRadioItemFactory;
use UnicaenUtilisateur\View\Helper\UserStatus;
use UnicaenUtilisateur\View\Helper\UserStatusFactory;

return [
    'doctrine' => [
        'driver' => [
            'orm_default' => [
                'class' => MappingDriverChain::class,
                'drivers' => [
                    'UnicaenUtilisateur\Entity\Db' => 'orm_default_xml_driver',
                ],
            ],
            'orm_default_xml_driver' => [
                'class' => XmlDriver::class,
                'cache' => 'apc',
                'paths' => [
                    __DIR__ . '/../src/UnicaenUtilisateur/Entity/Db/Mapping',
                ],
            ],
        ],
        'cache' => [
            'apc' => [
                'namespace' => 'UNICAEN-UTILISATEUR__' . __NAMESPACE__,
            ],
        ],
    ],

    'bjyauthorize'    => [

        /* role providers simply provide a list of roles that should be inserted
         * into the Zend\Acl instance. the module comes with two providers, one
         * to specify roles in a config file and one to load roles using a
         * Laminas\Db adapter.
         */
        'role_providers'    => [
            /**
             * 2 rôles doivent systématiquement exister dans les ACL :
             * - le rôle par défaut 'guest', c'est le rôle de tout utilisateur non authentifié.
             * - le rôle 'user', c'est le rôle de tout utilisateur authentifié.
             */
            'UnicaenUtilisateur\Provider\Role\Config'   => [
                'guest' => ['name' => "Non authentifié(e)", 'selectable' => false, 'children' => [
                'user' => ['name' => "Authentifié(e)", 'selectable' => false],
                ]],
            ],
        ],
    ],

    'service_manager' => [
        'invokables' => [],
        'factories' => [
            HistoriqueListener::class => HistoriqueListenerFactory::class,
        ],
    ],

    'view_manager'    => [
        'template_path_stack' => [
            __DIR__ . '/../view',
        ],
    ],

    'view_helpers'  => [
        'invokables' => [
            'role' => RoleViewHelper::class,
        ],
        'aliases' => [
            'userCurrent'                => UserCurrent::class,
            'userStatus'                 => UserStatus::class,
            'userProfile'                => UserProfile::class,
            'userInfo'                   => UserInfo::class,
            'userProfileSelect'          => UserProfileSelect::class,
            'userProfileSelectRadioItem' => UserProfileSelectRadioItem::class,
        ],
        'factories' => [
            UserConnection::class             => UserConnectionFactory::class,
            UserCurrent::class                => UserCurrentFactory::class,
            UserStatus::class                 => UserStatusFactory::class,
            UserProfile::class                => UserProfileFactory::class,
            UserInfo::class                   => UserInfoFactory::class,
            UserProfileSelect::class          => UserProfileSelectFactory::class,
            UserProfileSelectRadioItem::class => UserProfileSelectRadioItemFactory::class,
        ],
    ],
];
