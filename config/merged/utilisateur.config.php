<?php

use UnicaenUtilisateur\Controller\UtilisateurController;
use UnicaenUtilisateur\Controller\UtilisateurControllerFactory;
use UnicaenUtilisateur\Form\User\InitCompteForm;
use UnicaenUtilisateur\Form\User\UserForm;
use UnicaenUtilisateur\Form\User\UserFormFactory;
use UnicaenUtilisateur\Form\User\UserHydrator;
use UnicaenUtilisateur\Form\User\UserHydratorFactory;
use UnicaenUtilisateur\Form\User\UserRechercheForm;
use UnicaenUtilisateur\Provider\Privilege\UtilisateurPrivileges;
use UnicaenUtilisateur\Service\Role\RoleService;
use UnicaenUtilisateur\Service\Role\RoleServiceFactory;
use UnicaenUtilisateur\Service\User\UserService;
use UnicaenUtilisateur\Service\User\UserServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [

    'unicaen-utilisateur' => [
        /**
         * Liste des services utilisée pour rechercher un utilisateur existant ou à ajouter dans l'application.
         * Le service 'app' est défini par défaut et va rechercher dans la table des utilisateurs défini dans le paramètre 'user_entity_class'.
         */
        'recherche-individu' => [
            'app'       => UserService::class,
        ],

        /**
         * Permet d'afficher, en-dessous de la liste des rôles à sélectionner dans la popover
         * de l'utilisateur courant, les affectations principales, affectations fines, etc.
         */
        'display-user-info' => true,
    ],

    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => UtilisateurController::class,
                    'action' => [
                        'changer-mot-de-passe',
                    ],
                    'roles' => [
                    ],
                ],
                [
                    'controller' => UtilisateurController::class,
                    'action' => [
                        'index',
                        'rechercher',
                        'lister',
                        'exporter'
                    ],
                    'privileges' => [
                        UtilisateurPrivileges::UTILISATEUR_AFFICHER,
                    ],
                ],
                [
                    'controller' => UtilisateurController::class,
                    'action' => [
                        'supprimer',
                        'ajouter',
                    ],
                    'privileges' => [
                        UtilisateurPrivileges::UTILISATEUR_AJOUTER,
                    ],
                ],
                [
                    'controller' => UtilisateurController::class,
                    'action' => [
                        'changer-statut',
                    ],
                    'privileges' => [
                        UtilisateurPrivileges::UTILISATEUR_CHANGERSTATUS,
                    ],
                ],
                [
                    'controller' => UtilisateurController::class,
                    'action' => [
                        'add-role',
                        'remove-role',
                    ],
                    'privileges' => [
                        UtilisateurPrivileges::UTILISATEUR_MODIFIERROLE,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-utilisateur' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/utilisateur',
                    'defaults' => [
                        'controller' => UtilisateurController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'lister' => [
                        'type'          => Literal::class,
                        'options'       => [
                            'route'       => '/lister',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'lister',
                            ],
                        ],
                    ],
                    'ajouter' => [
                        'type'          => Literal::class,
                        'options'       => [
                            'route'       => '/ajouter',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'ajouter',
                            ],
                        ],
                    ],
                    'changer-mot-de-passe' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/changer-mot-de-passe/:user/:token',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'changer-mot-de-passe',
                            ],
                        ],
                    ],
                    'exporter' => [
                        'type'          => Literal::class,
                        'options'       => [
                            'route'       => '/exporter',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'exporter',
                            ],
                        ],
                    ],
                    'rechercher' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/rechercher',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'rechercher',
                            ],
                        ],
                    ],
                    'add-role' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/add-role/:utilisateur/:role',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'add-role',
                            ],
                        ],
                    ],
                    'remove-role' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/remove-role/:utilisateur/:role',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'remove-role',
                            ],
                        ],
                    ],
                    'changer-statut' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/changer-statut/:utilisateur',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'changer-statut',
                            ],
                        ],
                    ],
                    'supprimer' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/supprimer/:utilisateur',
                            'defaults'    => [
                                'controller' => UtilisateurController::class,
                                'action' => 'supprimer',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            UserService::class => UserServiceFactory::class,
            RoleService::class => RoleServiceFactory::class,
        ],
    ],

    'form_elements' => [
        'invokables' => [
            UserRechercheForm::class => UserRechercheForm::class,
            InitCompteForm::class => InitCompteForm::class,
        ],
        'factories' => [
            UserForm::class => UserFormFactory::class,
        ],
    ],

    'hydrators' => [
        'factories' => [
            UserHydrator::class => UserHydratorFactory::class,
        ]
    ],
    
    'controllers'     => [
        'factories' => [
            UtilisateurController::class => UtilisateurControllerFactory::class,
        ]
    ],
];
