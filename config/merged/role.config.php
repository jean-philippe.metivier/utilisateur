<?php

use UnicaenUtilisateur\Controller\RoleController;
use UnicaenUtilisateur\Controller\RoleControllerFactory;
use UnicaenUtilisateur\Form\Role\RoleForm;
use UnicaenUtilisateur\Form\Role\RoleFormFactory;
use UnicaenUtilisateur\Form\Role\RoleHydrator;
use UnicaenUtilisateur\Form\Role\RoleHydratorFactory;
use UnicaenUtilisateur\Service\Role\RoleService;
use UnicaenUtilisateur\Service\Role\RoleServiceFactory;
use UnicaenUtilisateur\Provider\Privilege\RolePrivileges;
use UnicaenUtilisateur\Provider\Role\Config;
use UnicaenUtilisateur\Provider\Role\ConfigServiceFactory;
use UnicaenUtilisateur\Provider\Role\DbRole;
use UnicaenUtilisateur\Provider\Role\DbRoleServiceFactory;
use UnicaenUtilisateur\Provider\Role\Username;
use UnicaenUtilisateur\Provider\Role\UsernameServiceFactory;
use UnicaenPrivilege\Guard\PrivilegeController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return [
    'bjyauthorize' => [
        'guards' => [
            PrivilegeController::class => [
                [
                    'controller' => RoleController::class,
                    'action' => [
                        'index',
                        'exporter',
                    ],
                    'privileges' => [
                        RolePrivileges::ROLE_AFFICHER,
                    ],
                ],
                [
                    'controller' => RoleController::class,
                    'action' => [
                        'ajouter',
                        'modifier',
                    ],
                    'privileges' => [
                        RolePrivileges::ROLE_MODIFIER,
                    ],
                ],
                [
                    'controller' => RoleController::class,
                    'action' => [
                        'supprimer',
                    ],
                    'privileges' => [
                        RolePrivileges::ROLE_EFFACER,
                    ],
                ],
            ],
        ],
    ],

    'router' => [
        'routes' => [
            'unicaen-role' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/role',
                    'defaults' => [
                        'controller' => RoleController::class,
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'exporter' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/exporter/:role',
                            'defaults'    => [
                                'controller' => RoleController::class,
                                'action' => 'exporter',
                            ],
                        ],
                    ],
                    'ajouter' => [
                        'type'          => Literal::class,
                        'options'       => [
                            'route'       => '/ajouter',
                            'defaults'    => [
                                'controller' => RoleController::class,
                                'action' => 'ajouter',
                            ],
                        ],
                    ],
                    'modifier' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/modifier/:role',
                            'defaults'    => [
                                'controller' => RoleController::class,
                                'action' => 'modifier',
                            ],
                        ],
                    ],
                    'supprimer' => [
                        'type'          => Segment::class,
                        'options'       => [
                            'route'       => '/supprimer/:role',
                            'defaults'    => [
                                'controller' => RoleController::class,
                                'action' => 'supprimer',
                            ],
                        ],
                    ],
                ],
            ],
        ],
    ],

    'service_manager' => [
        'factories' => [
            Config::class => ConfigServiceFactory::class,
            DbRole::class => DbRoleServiceFactory::class,
            Username::class => UsernameServiceFactory::class,
            RoleService::class => RoleServiceFactory::class,
        ],
    ],
    'controllers'     => [
        'factories' => [
            RoleController::class => RoleControllerFactory::class,
        ],
    ],
    'form_elements' => [
        'factories' => [
            RoleForm::class => RoleFormFactory::class,
        ],
    ],
    'hydrators' => [
        'factories' => [
            RoleHydrator::class => RoleHydratorFactory::class,
        ],
    ],
];
