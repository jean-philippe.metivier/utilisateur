Journal des modifications
=========================

6.0.4 (21/03/2023)
-----
- Ajout du paramètre [display-user-info](config/unicaen-utilisateur.global.php.dist) qui permet d'afficher ou nonles affectations LDAP de l'utilisateur courant
- [Fix] Test de présence de 'userUsurpation' pour couplage faible avec UnicaenAuthentification

5.0.8
------
* Correction du formulaire des rôles (la decsription n'est plus obligatoire)
* Création d'une classe abstraite AbstractIdentityProvider pour somplifire la déclaration des IdentityProviders
* Ajout de garde à l'appel des fonctions de gestion des rôles automatiques 

5.0.7 
------
* Ajout du champs description pour les rôles
* Ajout d'un view helper pour l'affichage des rôles ```echo $this->role($role)```
* Début de nettoyage des classes vers la syntaxe PHP8


0.0.2 (29/03/2021)
-----
- [Fix] Correction exception incomplete dans UserService

0.0.1 (12/03/2021)
-----

Version initiale du module permettant :
- la gestion des utilisateurs
- la gestion des rôles
- l'affectation de rôles à des utilisateurs
- l'export des utilisateurs
- les exports des utilisateurs ayant un rôle donné 
