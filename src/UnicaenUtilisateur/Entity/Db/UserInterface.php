<?php

namespace UnicaenUtilisateur\Entity\Db;

use Doctrine\Common\Collections\Collection;

interface UserInterface extends \ZfcUser\Entity\UserInterface
{
    /**
     * @return string
     */
    public function __toString();

    /**
     * Get passwordResetToken.
     *
     * @return string|null
     */
    public function getPasswordResetToken();

    /**
     * Set passwordResetToken.
     *
     * @param string|null $passwordResetToken
     * @return UserInterface
     */
    public function setPasswordResetToken($passwordResetToken);

    /**
     * Get lastRole.
     *
     * @return RoleInterface|null
     */
    public function getLastRole();

    /**
     * Set lastRole.
     *
     * @param RoleInterface|null $lastRole
     * @return UserInterface
     */
    public function setLastRole(?RoleInterface $lastRole);

    /**
     * Get roles of user
     *
     * @return Collection
     */
    public function getRoles();

    /**
     * Add a role to user.
     *
     * @param RoleInterface $role
     * @return UserInterface
     */
    public function addRole(RoleInterface $role);

    /**
     * Remove a role to user.
     *
     * @param RoleInterface $role
     * @return UserInterface
     */
    public function removeRole(RoleInterface $role);

    /**
     * Check user role
     *
     * @param RoleInterface $role
     * @return bool
     */
    function hasRole(RoleInterface $role);

    /**
     * Check if is local user
     *
     * @return bool
     */
    public function isLocal();
}