<?php

namespace UnicaenUtilisateur\Entity\Db\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use UnicaenApp\Service\EntityManagerAwareTrait;

class RoleRepository extends EntityRepository
{
    use EntityManagerAwareTrait;

    public function createQueryBuilder($alias, $indexBy = null): QueryBuilder
    {
        $qb = parent::createQueryBuilder($alias, $indexBy);
        $qb
            ->leftjoin($alias . '.privileges', 'privilege')->addSelect('privilege')
            ->leftJoin('privilege.categorie', 'categorie')->addSelect('categorie')
        ;
        return $qb;
    }

    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $qb = $this->createQueryBuilder('role')
            ->andWhere('role.id = :id')->setParameter('id', $id);
        $result = $qb->getQuery()->getOneOrNullResult();
        return $result;
    }

}