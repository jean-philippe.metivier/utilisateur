<?php

namespace UnicaenUtilisateur\Entity\Db;

use BjyAuthorize\Provider\Role\ProviderInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuResultatInterface;

abstract class AbstractUser implements UserInterface, ProviderInterface, RechercheIndividuResultatInterface
{
    const PASSWORD_LDAP = 'ldap';
    const PASSWORD_SHIB = 'shib';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $username;

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $displayName;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var int
     */
    protected $state;

    /**
     * @var string
     */
    protected $passwordResetToken;

    /**
     * @var RoleInterface
     */
    private $lastRole;

    /**
     * @var ArrayCollection
     */
    protected $roles;


    /**
     * AbstractUser constructor.
     */
    public function __construct()
    {
        $this->roles = new ArrayCollection();
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return (string)$this->getDisplayName();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set id.
     *
     * @param int $id
     * @return UserInterface
     */
    public function setId($id)
    {
        $this->id = (int) $id;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string
     */
    public function getUsername(string $attribut = "supannAliasLogin")
    {
        return $this->username;
    }

    /**
     * Set username.
     *
     * @param string $username
     * @return UserInterface
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     * @return UserInterface
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get displayName.
     *
     * @return string|null
     */
    public function getDisplayName()
    {
        return $this->displayName;
    }

    /**
     * Set displayName.
     *
     * @param string|null $displayName
     * @return UserInterface
     */
    public function setDisplayName($displayName)
    {
        $this->displayName = $displayName;

        return $this;
    }

    /**
     * Get password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set password.
     *
     * @param string $password
     * @return UserInterface
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get state.
     *
     * @return int
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set state.
     *
     * @param int $state
     * @return UserInterface
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get passwordResetToken.
     *
     * @return string|null
     */
    public function getPasswordResetToken()
    {
        return $this->passwordResetToken;
    }

    /**
     * Set passwordResetToken.
     *
     * @param string|null $passwordResetToken
     * @return UserInterface
     */
    public function setPasswordResetToken($passwordResetToken)
    {
        $this->passwordResetToken = $passwordResetToken;

        return $this;
    }

    /**
     * Get lastRole.
     *
     * @return RoleInterface|null
     */
    public function getLastRole()
    {
        return $this->lastRole;
    }

    /**
     * Set lastRole.
     *
     * @param RoleInterface|null $lastRole
     * @return UserInterface
     */
    public function setLastRole(?RoleInterface $lastRole)
    {
        $this->lastRole = $lastRole;

        return $this;
    }

    /**
     * Get roles of user
     *
     * @return Collection
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * Add a role to user.
     *
     * @param RoleInterface $role
     * @return UserInterface
     */
    public function addRole(RoleInterface $role)
    {
        $this->roles->add($role);

        return $this;
    }

    /**
     * Remove a role to user.
     *
     * @param $role
     * @return UserInterface
     */
    public function removeRole(RoleInterface $role)
    {
        $this->roles->removeElement($role);

        return $this;
    }

    /**
     * Check user role
     *
     * @param RoleInterface $role
     * @return bool
     */
    function hasRole(RoleInterface $role)
    {
        foreach ($this->roles as $r) {
            if ($r === $role) return true;
        }

        return false;
    }

    /**
     * Retourne true si cet utilisateur est local.
     *
     * Un utilisateur est local s'il ne résulte pas d'une authentification LDAP ou Shibboleth.
     * Son mot de passe est chiffré dans la table des utilisateurs.
     *
     * @return bool
     */
    public function isLocal()
    {
        return ! in_array($this->getPassword(), [
            AbstractUser::PASSWORD_LDAP,
            AbstractUser::PASSWORD_SHIB,
        ]);
    }
}