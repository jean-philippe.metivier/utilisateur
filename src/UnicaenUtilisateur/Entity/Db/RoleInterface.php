<?php

namespace UnicaenUtilisateur\Entity\Db;

use BjyAuthorize\Acl\HierarchicalRoleInterface;
use Doctrine\Common\Collections\Collection;
use UnicaenPrivilege\Entity\Db\PrivilegeInterface;

interface RoleInterface extends HierarchicalRoleInterface
{
    public function __toString() : string;

    public function getId() : ?int;
    public function setId(?int $id) : void;
    public function getRoleId() : ?string;
    public function setRoleId(?string $roleId) : void;
    public function getLibelle() : ?string;
    public function setLibelle(?string $libelle) : void;
    public function getDescription() : ?string;
    public function setDescription(?string $description) : void;

    public function isDefault() : bool;
    public function setDefault(bool $default) : void;
    public function isAuto() : bool;
    public function setAuto(bool $auto) : void;
    public function isAccessibleExterieur() : bool;
    public function setAccessibleExterieur(bool $accessibleExterieur) : void;

    public function getParent() : ?RoleInterface;
    public function setParent(?RoleInterface $parent = null) : void;
    public function getLdapFilter() : ?string;
    public function setLdapFilter(?string $ldapFilter) : void;

    public function getUsers() : Collection;
    public function addUser(UserInterface $user) : void;
    public function removeUser(UserInterface $user) : void;

    public function getPrivileges();
    public function addPrivilege(PrivilegeInterface $privilege) : void;
    public function removePrivilege(PrivilegeInterface $privilege) : void;
}