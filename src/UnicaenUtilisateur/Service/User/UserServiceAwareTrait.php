<?php

namespace UnicaenUtilisateur\Service\User;

use RuntimeException;

trait UserServiceAwareTrait
{
    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @param UserService $userService
     * @return UserService
     */
    public function setUserService(UserService $userService): UserService
    {
        $this->userService = $userService;

        return $this->userService;
    }

    /**
     * @return UserService
     */
    public function getUserService(): UserService
    {
        return $this->userService;
    }
}

