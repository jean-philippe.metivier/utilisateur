<?php

namespace UnicaenUtilisateur\Service\User;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\ORMException;
use InvalidArgumentException;
use Ramsey\Uuid\Uuid;
use UnicaenApp\Entity\Ldap\People;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenAuthentification\Service\Traits\UserContextServiceAwareTrait;
use UnicaenMail\Service\Mail\MailServiceAwareTrait;
use UnicaenUtilisateur\Controller\UtilisateurController;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenUtilisateur\Exception\RuntimeException;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuResultatInterface;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuServiceInterface;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use Laminas\Authentication\AuthenticationService;
use Laminas\Crypt\Password\Bcrypt;
use Laminas\Mvc\Controller\AbstractActionController;

class UserService implements RechercheIndividuServiceInterface
{
    use EntityManagerAwareTrait;
    use UserContextServiceAwareTrait;
    use RoleServiceAwareTrait;
    use MailServiceAwareTrait;

    private ?string $userEntityClass = null;
    private ?array $authentificationConfig = null;

    public function getAuthentificationConfig(): ?array
    {
        return $this->authentificationConfig;
    }

    public function setAuthentificationConfig(?array $authentificationConfig): void
    {
        $this->authentificationConfig = $authentificationConfig;
    }

    private ?AuthenticationService $authenticationService;

    private $renderer;
    public function setRenderer($renderer) { $this->renderer = $renderer;}
    private ?string $appname = null;
    public function setAppname($appname) { $this->appname = $appname;}
    

    /**
     * @param string $userEntityClass
     * @return void
     */
    public function setUserEntityClass(string $userEntityClass) : void
    {
        if (! class_exists($userEntityClass) || ! in_array(UserInterface::class, class_implements($userEntityClass))) {
            throw new InvalidArgumentException("L'entité associée aux utilisateurs doit implémenter " . UserInterface::class);
        }
        $this->userEntityClass = $userEntityClass;
    }

    /**
     * @return string
     */
    public function getEntityClass() : ?string
    {
        return $this->userEntityClass;
    }

    /**
     * Retourne une nouvelle instance de l'entité lié au service
     *
     * @return UserInterface
     */
    public function getEntityInstance() : UserInterface
    {
        return new $this->userEntityClass;
    }

    /**
     * @param AuthenticationService $authenticationService
     * @return void
     */
    public function setAuthenticationService(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @return EntityRepository
     */
    public function getRepo()
    {
        return $this->getEntityManager()->getRepository($this->userEntityClass);
    }

    /**
     * @param mixed $id
     * @return UserInterface|null
     */
    public function find($id)
    {
        return $this->getRepo()->find($id);
    }

    /**
     * @return UserInterface[]
     */
    public function findAll(array $orderBy = ['displayName' => 'ASC'])
    {
        return $this->getRepo()->findBy([], $orderBy);
    }

    /**
     * @param string $username
     * @return UserInterface|null
     */
    public function findByUsername($username)
    {
        return $this->getRepo()->findOneBy(['username' => $username]);
    }

    /**
     * @param $id
     * @return UserInterface
     */
    public function findById($id)
    {
        return $this->find($id);
    }
    
    /**
     * @param string $texte
     * @return UserInterface[]
     */
    public function findByTerm(string $texte)
    {
        $qb = $this->getRepo()->createQueryBuilder("u");
        $qb->andWhere(
                $qb->expr()->orX(
                    "LOWER(u.displayName) LIKE :term",
                    "LOWER(u.username) LIKE :term"
                )
            )
            ->setParameter("term", '%' . strtolower($texte) . '%')
            ->orderBy("u.displayName");

        $utilisateurs = $qb->getQuery()->getResult();

        return $utilisateurs;
    }

    /**
     * @param string $texte
     * @param string $roleId
     * @return UserInterface[]
     */
    public function findByTermAndRole(string $texte, string $roleId)
    {
        $texte = strtolower($texte);
        $qb = $this->getRepo()->createQueryBuilder("u")
            ->andWhere("LOWER(u.displayName) LIKE :critere")
            ->setParameter("critere", '%'.strtolower($texte).'%')
            ->addSelect('r')->join('u.roles', 'r')
            ->andWhere('r.roleId = :role')
            ->setParameter("role", $roleId)
            ->orderBy("u.displayName");

        $utilisateurs = $qb->getQuery()->getResult();

        return $utilisateurs;
    }

    /**
     * @param RechercheIndividuResultatInterface $individu
     * @param string $source
     * @return UserInterface
     */
    public function importFromRechercheIndividuResultatInterface(RechercheIndividuResultatInterface $individu, $source)
    {
        $attribut = $this->getAuthentificationConfig()['local']['ldap']['username'];

        $utilisateur = $this->getEntityInstance();
        $utilisateur->setDisplayName($individu->getDisplayname());
        $utilisateur->setUsername($individu->getUsername($attribut));
        $utilisateur->setEmail($individu->getEmail());
        $utilisateur->setPassword($source);
        $utilisateur->setState(1);

        return $utilisateur;
    }

    public function getRequestedUser(AbstractActionController $controller, string $paramName = 'utilisateur') : ?UserInterface
    {
        $id = $controller->params()->fromRoute($paramName);
        if ($id === null) return null;
        $user = $this->find($id);

        return $user;
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function create(UserInterface $user)
    {
        try {
            $this->getEntityManager()->persist($user);
            $this->getEntityManager()->flush($user);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la création de l'utilisateur.", null, $e);
        }
        return $user;
    }

    /**
     * @param UserInterface $user
     * @return UserInterface
     */
    public function createLocal(UserInterface $user)
    {
        $user->setState(1);
        $user->setPassword('db');
        $token = Uuid::uuid4()->toString();
        $user->setPasswordResetToken($token);
        $this->create($user);


        $appname = $this->appname;
        /** @see UtilisateurController::changerMotDePasseAction() */
        $url = $this->renderer->url('unicaen-utilisateur/changer-mot-de-passe', ['user' => $user->getId(), 'token' => $user->getPasswordResetToken()], ['force_canonical' => true], true);
        $subject = "Initialisation de votre compte pour l'application".$appname;
        $body="";
        $body.="<p>Bonjour ".$user->getDisplayName().",</p>";
        $body.="<p>Pour initialiser votre mot de passe pour l'application ".$appname." veuillez suivre le lien suivant <a href='".$url."'>".$url."</a>.</p>";
        $body.="<p>Vous aurez besoin de votre identifiant qui est <strong>".$user->getUsername()."</strong></p>";
        $this->getMailService()->sendMail($user->getEmail(), $subject, $body);
        
        return $user;
    }

    public function changerPassword(User $user, $data) : User
    {
        $password = $data['password1'];

        $bcrypt = new Bcrypt();
//        $bcrypt->setCost($this->userService->getZfcUserOptions()->getPasswordCost());
        $password = $bcrypt->create($user->getPassword());
        $user->setPassword($password);
        $user->setPasswordResetToken(null);
        $this->update($user);

        return $user;
    }

    /**
     * @param UserInterface $utilisateur
     * @return UserInterface
     */
    public function update(UserInterface $utilisateur)
    {
        try {
            $this->getEntityManager()->flush($utilisateur);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la mise à jour de l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }
        return $utilisateur;
    }

    /**
     * @param UserInterface $utilisateur
     * @return UserInterface
     */
    public function changeStatus(UserInterface $utilisateur)
    {
        $status = $utilisateur->getState();
        $utilisateur->setState($status == 1 ? 0 : 1);

        try {
            $this->getEntityManager()->flush($utilisateur);
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors du changement de statut de l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }

        return $utilisateur;
    }

    /**
     * @param UserInterface $utilisateur
     * @param RoleInterface $role
     * @return UserInterface
     */
    public function addRole(UserInterface $utilisateur, RoleInterface $role)
    {
        $utilisateur->addRole($role);

        try {
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de l'ajout du rôle \"" . $role->getLibelle() . "\" à l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }

        return $utilisateur;
    }

    /**
     * @param UserInterface $utilisateur
     * @param RoleInterface $role
     * @return UserInterface
     */
    public function removeRole(UserInterface $utilisateur, RoleInterface $role)
    {
        $utilisateur->removeRole($role);

        try {
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la suppression du rôle \"" . $role->getLibelle() . "\" à l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }

        return $utilisateur;
    }

    /**
     * @param UserInterface $utilisateur
     */
    public function delete(UserInterface $utilisateur)
    {
        try {
            $this->getEntityManager()->remove($utilisateur);
            $this->getEntityManager()->flush();
        } catch (ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la suppression de l'utilisateur " . $utilisateur->getUsername() . ".", null, $e);
        }
    }

    /**
     * @param RoleInterface $role
     * @return UserInterface[]
     */
    public function findByRole(RoleInterface $role)
    {
        $qb = $this->getRepo()->createQueryBuilder('u')
            ->addSelect('r')->join('u.roles', 'r')
            ->andWhere('r.roleId = :role')
            ->setParameter('role', $role->getRoleId());

        return $qb->getQuery()->getResult();
    }

    public function getConnectedUser() : ?UserInterface
    {
        $identity = $this->authenticationService->getIdentity();

        if ($identity) {
            if (isset($identity['ldap'])) {
                /** @var People $userIdentity */
                $userIdentity = $identity['ldap'];
                switch ($this->getAuthentificationConfig()['local']['ldap']['username']) {
                    case 'uid' :
                        $username = $userIdentity->getUid();
                        break;
                    case 'supannaliaslogin' :
                        $username = $userIdentity->getSupannAliasLogin();
                        break;
                    default :
                        throw new RuntimeException("La clef de config [unicaen-auth][loca][ldap][username] n'est pas défini ou à une valeur non attendue.");
                }

                $user = $this->findByUsername($username);

                return $user;
            }
            if (isset($identity['shib'])) {
                /** @var People $userIdentity */
                $userIdentity = $identity['shib'];
                $username = $userIdentity->getUsername();
                $user = $this->findByUsername($username);

                return $user;
            }
            if (isset($identity['db'])) {
                return $identity['db'];
            }
        }

        return null;
    }

    /**
     * @return \Laminas\Permissions\Acl\Role\RoleInterface|null
     */
    public function getConnectedRole()
    {
        $dbRole = $this->serviceUserContext->getSelectedIdentityRole();

        return $dbRole;
    }

    /**
     * @param $string
     * @return array
     */
    public function getUtilisateursByRoleIdAsOptions($string)
    {
        $role = $this->roleService->findByRoleId($string);
        $users = $this->findByRole($role);

        $array = [];
        foreach ($users as $user) {
            $array[$user->getId()] = $user->getDisplayName();
        }

        return $array;
    }

    /**
     * @param UserInterface[] $users
     * @return array
     */
    public function formatUserJSON($users)
    {
        $result = [];

        /** @var User[] $responsables */
        foreach ($users as $user) {
            $result[] = array(
                'id'    => $user->getId(),
                'label' => $user->getDisplayName(),
                'extra' => "<span class='badge' style='background-color: slategray;'>".$user->getEmail()."</span>",
            );
        }
        usort($result, function($a, $b) {
            return strcmp($a['label'], $b['label']);
        });

        return $result;
    }

    /** TODO FAIRE MIEUX */
    public function getMaxId() : int
    {
        /** @var User $users */
        $users = $this->getRepo()->findAll();
        $maximum = 0;

        foreach ($users as $user) {
            if ($user->getId() > $maximum) $maximum = $user->getId();
        }
        return $maximum;
    }
}

