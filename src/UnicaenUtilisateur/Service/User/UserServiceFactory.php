<?php

namespace UnicaenUtilisateur\Service\User;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use Laminas\View\Renderer\PhpRenderer;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenAuthentification\Service\UserContext;
use UnicaenMail\Service\Mail\MailService;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Service\Role\RoleService;
use Laminas\Authentication\AuthenticationService;

class UserServiceFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UserService
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null) : UserService
    {
        /**
         * @var EntityManager $entityManager
         * @var MailService $mailService
         * @var RoleService $roleService
         * @var UserContext $userContext
         * @var AuthenticationService $authenticationService
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $mailService = $container->get(MailService::class);
        $roleService = $container->get(RoleService::class);
        $userContext = $container->get(UserContext::class);
        $allConfig = $container->get('Config');
        $authenticationService = $container->get('Laminas\Authentication\AuthenticationService');

        $service = new UserService();
        $service->setEntityManager($entityManager);
        $service->setMailService($mailService);
        $service->setRoleService($roleService);
        $service->setServiceUserContext($userContext);
        $service->setUserEntityClass($allConfig['zfcuser']['user_entity_class'] ?? User::class);
        $service->setAuthentificationConfig($allConfig['unicaen-auth']);
        $service->setAuthenticationService($authenticationService);

        /* @var PhpRenderer $renderer  */
        $renderer = $container->get('ViewRenderer');
        $service->setRenderer($renderer);

        $config = $container->get('Config');
        $appname = $config['unicaen-app']['app_infos']['nom'];
        $service->setAppname($appname);
        
        return $service;
    }
}