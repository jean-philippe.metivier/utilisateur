<?php 

namespace UnicaenUtilisateur\Service\RechercheIndividu;

interface RechercheIndividuResultatInterface
{
    /**
     * @return mixed
     */
    public function getId();
    
    /**
     * @return string
     */
    public function getUsername(string $attribut = "supannAliasLogin");

    /**
     * @return string
     */
    public function getDisplayname();

    /**
     * @return string
     */
    public function getEmail();
}