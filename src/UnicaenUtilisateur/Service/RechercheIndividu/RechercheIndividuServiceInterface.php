<?php

namespace UnicaenUtilisateur\Service\RechercheIndividu;

interface RechercheIndividuServiceInterface
{
    /** 
     * @param string $term
     * @return RechercheIndividuResultatInterface[] 
     */
    public function findByTerm(string $term);

    /** 
     * @param mixed $id
     * @return RechercheIndividuResultatInterface 
     */
    public function findById($id);
} 