<?php

namespace UnicaenUtilisateur\Service\Role;

use Doctrine\ORM\EntityManager;
use Interop\Container\ContainerInterface;
use UnicaenUtilisateur\Entity\Db\Role;

class RoleServiceFactory
{
    /**
     * Create service
     *
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return RoleService
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var EntityManager $entityManager
         */
        $entityManager = $container->get('Doctrine\ORM\EntityManager');
        $config = $container->get('Config');

        $service = new RoleService();
        $service->setEntityManager($entityManager);
        $service->setRoleEntityClass($config['unicaen-auth']['role_entity_class'] ?? Role::class);


        return $service;
    }
}