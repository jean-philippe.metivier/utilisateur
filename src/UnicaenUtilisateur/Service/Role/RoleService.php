<?php

namespace UnicaenUtilisateur\Service\Role;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use InvalidArgumentException;
use Laminas\Mvc\Controller\AbstractActionController;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use UnicaenUtilisateur\Exception\RuntimeException;

class RoleService
{
    use EntityManagerAwareTrait;

    /**
     * @var string
     */
    private $roleEntityClass;


    /**
     * @param string $roleEntityClass
     * @return void
     */
    public function setRoleEntityClass(string $roleEntityClass)
    {
        if (! class_exists($roleEntityClass) || ! in_array(RoleInterface::class, class_implements($roleEntityClass))) {
            throw new InvalidArgumentException("L'entité associée aux rôles doit implémenter " . RoleInterface::class);
        }

        $this->roleEntityClass = $roleEntityClass;
    }

    /**
     * @return string
     */
    public function getEntityClass() : string
    {
        return $this->roleEntityClass;
    }

    /**
     * Retourne une nouvelle instance de l'entité lié au service
     *
     * @return RoleInterface
     */
    public function getEntityInstance()
    {
        return new $this->roleEntityClass;
    }

    /**
     * @return EntityRepository
     */
    public function getRepo()
    {
        return $this->getEntityManager()->getRepository($this->roleEntityClass);
    }

    /**
     * @param ?int $id
     * @return RoleInterface|null
     */
    public function find(?int $id)
    {
        return $this->getRepo()->find($id);
    }

    /**
     * @return RoleInterface[]
     */
    public function findAll(array $orderBy = ['roleId' => 'ASC'])
    {
        return $this->getRepo()->findBy([], $orderBy);
    }

    /**
     * @param string $roleId
     * @return RoleInterface|null
     */
    public function findByRoleId(string $roleId) : ?RoleInterface
    {
        return $this->getRepo()->findOneBy(['roleId' => $roleId]);
    }

    /**
     * @param string $libelle
     * @return RoleInterface|null
     */
    public function findByLibelle(string $libelle) : ?RoleInterface
    {
        return $this->getRepo()->findOneBy(['libelle' => $libelle]);
    }

    /**
     * @return array
     */
    public function getRolesAsOptions() : array
    {
        $roles = $this->findAll();
        $result = [];

        foreach ($roles as $role) {
            $result[$role->getId()] = $role->getLibelle();
        }

        return $result;
    }

    /**
     * @param AbstractActionController $controller
     * @param string $paramName
     * @return RoleInterface
     */
    public function getRequestedRole(AbstractActionController $controller, string $paramName = 'role') : RoleInterface
    {
        $id = $controller->params()->fromRoute($paramName);
        $role = $this->find($id);

        return $role;
    }

    /**
     * @param RoleInterface $role
     * @return RoleInterface
     */
    public function create(RoleInterface $role) : RoleInterface
    {
        try {
            $this->getEntityManager()->persist($role);
            $this->getEntityManager()->flush($role);
        } catch(ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la création du rôle.", null, $e);
        }

        return $role;
    }

    /**
     * @param RoleInterface $role
     * @return RoleInterface
     */
    public function update(RoleInterface $role) : RoleInterface
    {
        try {
            $this->getEntityManager()->flush($role);
        } catch(ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la mise à jour du rôle \"" . $role->getLibelle() . "\"", null, $e);
        }

        return $role;
    }

    /**
     * @param RoleInterface $role
     * @return RoleInterface
     */
    public function delete(RoleInterface $role) : RoleInterface
    {
        try {
            $this->getEntityManager()->remove($role);
            $this->getEntityManager()->flush($role);
        } catch(ORMException $e) {
            throw new RuntimeException("Un problème est survenu lors de la suppression du rôle \"" . $role->getLibelle() . "\"", null, $e);
        }

        return $role;
    }

}