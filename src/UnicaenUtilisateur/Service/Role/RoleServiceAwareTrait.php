<?php

namespace UnicaenUtilisateur\Service\Role;

trait RoleServiceAwareTrait
{
    /**
     * @var RoleService
     */
    protected $roleService;

    /**
     * @param RoleService $serviceRole
     * @return RoleService
     */
    public function setRoleService(RoleService $serviceRole): RoleService
    {
        $this->roleService = $serviceRole;

        return $this->roleService;
    }

    /**
     * @return RoleService
     */
    public function getRoleService(): RoleService
    {
        return $this->roleService;
    }
}