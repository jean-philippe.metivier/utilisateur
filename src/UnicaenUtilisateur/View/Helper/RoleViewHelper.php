<?php

namespace UnicaenUtilisateur\View\Helper;

use Laminas\View\Helper\AbstractHelper;
use Laminas\View\Helper\Partial;
use Laminas\View\Renderer\PhpRenderer;
use Laminas\View\Resolver\TemplatePathStack;
use Prestation\Entity\Db\Type;
use UnicaenUtilisateur\Entity\Db\RoleInterface;

class RoleViewHelper  extends AbstractHelper
{
    /**
     * @param RoleInterface|null $role
     * @param array $options
     * @return string|Partial
     *
     * options :: tooltip
     */
    public function __invoke(RoleInterface $role, array $options = [])
    {
        /** @var PhpRenderer $view */
        $view = $this->getView();
        $view->resolver()->attach(new TemplatePathStack(['script_paths' => [__DIR__ . "/partial"]]));

        return $view->partial('role', ['role' => $role, 'options' => $options]);
    }
}