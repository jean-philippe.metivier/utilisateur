<?php

namespace UnicaenUtilisateur\View\Helper;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Description of UserCurrentFactory
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier at unicaen.fr>
 */
class UserCurrentFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $helperPluginManager)
    {
        return $this->__invoke($helperPluginManager, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $authUserContext = $container->get('authUserContext');

        $config = $container->get('config')['unicaen-utilisateur'] ?? [];
        $displayUserInfo = $config['display-user-info'] ?? true;

        $userCurrent = new UserCurrent($authUserContext);
        $userCurrent->setDisplayUserInfo($displayUserInfo);

        return $userCurrent;
    }
}