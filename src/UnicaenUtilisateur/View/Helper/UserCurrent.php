<?php
namespace UnicaenUtilisateur\View\Helper;

use Laminas\View\Renderer\PhpRenderer;

/**
 * Aide de vue affichant toutes les infos concernant l'utilisateur courant.
 * C'est à dire :
 *  - "Aucun" + lien de connexion OU BIEN nom de l'utilisateur connecté + lien de déconnexion
 *  - profil de l'utilisateur connecté
 *  - infos administratives sur l'utilisateur
 *
 * @author Bertrand GAUTHIER <bertrand.gauthier@unicaen.fr>
 */
class UserCurrent extends UserAbstract
{
    /**
     * @var bool
     */
    protected $affectationFineSiDispo = false;

    protected bool $displayUserInfo = true;

    /**
     * Point d'entrée.
     * 
     * @param boolean $affectationFineSiDispo Indique s'il faut prendre en compte l'affectation
     * plus fine (ucbnSousStructure) si elle existe, à la place de l'affectation standard (niveau 2)
     * @return self 
     */
    public function __invoke($affectationFineSiDispo = false)
    {
        $this->setAffectationFineSiDispo($affectationFineSiDispo);
        return $this;
    }
    
    /**
     * Retourne le code HTML généré par cette aide de vue.
     * 
     * @return string 
     */
    public function __toString(): string
    {
        $id = 'user-current-info';

        $userStatusHelper = $this->getView()->plugin('userStatus'); /* @var $userStatusHelper UserStatus */
        $status = $userStatusHelper(false);

        $userProfileSelectable = true;
        
        if ($this->getIdentity()) {
            if ($userProfileSelectable) {
		        $role = $this->getUserContext()->getSelectedIdentityRole();
                // cas où aucun rôle n'est sélectionné : on affiche le 1er rôle sélectionnable ou sinon "user"
		        if ($role === null) {
		            $selectableRoles = $this->getUserContext()->getSelectableIdentityRoles();
                    $role = current($selectableRoles) ?: $this->getUserContext()->getIdentityRole('user');
                }
                $status .= sprintf(", <small class='role-libelle'>%s</small>", !method_exists($role, '__toString') ? $role->getRoleId() : $role);
            }
        
            $userProfileHelper = $this->getView()->plugin('userProfile'); /* @var $userProfileHelper UserProfile */
            $userProfileHelper->setUserProfileSelectable($userProfileSelectable);
            
            $userInfoHelper = $this->getView()->plugin('userInfo'); /* @var $userInfoHelper UserInfo */
            
            $content = $userProfileHelper;

            // Affichage des affectations
            if ($this->isDisplayUserInfo()) {
                $content .= $userInfoHelper($this->getAffectationFineSiDispo());
            }

            // formulaire d'usurpation d'identité
            /** @var PhpRenderer $renderer */
            $renderer = $this->getView();

            // teste si userUsurpation de UnicaenAuthentification est dispo sans avoir de dépendance forte avec cette bib
            if ($renderer->getHelperPluginManager()->has('userUsurpation')) {
                /* @var  \UnicaenAuthentification\View\Helper\UserUsurpationHelper $userUsurpationHelper */
                $userUsurpationHelper = $renderer->plugin('userUsurpation');
                $content              .= $userUsurpationHelper;
            }
        }
        else {
            $content = _("Aucun");
            if ($this->getTranslator()) {
                $content = $this->getTranslator()->translate($content, $this->getTranslatorTextDomain());
            }
        }

        $content = htmlspecialchars(preg_replace('/\r\n|\n|\r/', '', $content));

        $title = _("Utilisateur connecté à l'application");
        if ($this->getTranslator()) {
            $title = $this->getTranslator()->translate($title, $this->getTranslatorTextDomain());
        }

        $out = <<<EOS
<a class="navbar-link" 
   id="$id" 
   title="$title" 
   data-bs-placement="bottom" 
   data-bs-toggle="popover" 
   data-bs-html="true" 
   data-bs-content="$content" 
   href="#">$status<span class="caret"></span></a>
EOS;
        $out .= PHP_EOL;

        $js = <<<EOS
$(function() {
    $("#$id").popover({ html: true, sanitize: false, container: '#navbar' });
});
EOS;
        $this->getView()->plugin('inlineScript')->offsetSetScript(1000, $js);

        return $out;
    }

    /**
     * Indique si l'affichage de l'affectation fine éventuelle est activé ou non.
     * 
     * @return bool
     */
    public function getAffectationFineSiDispo()
    {
        return $this->affectationFineSiDispo;
    }

    /**
     * Active ou non l'affichage de l'affectation fine éventuelle.
     * 
     * @param bool $affectationFineSiDispo
     * @return self
     */
    public function setAffectationFineSiDispo($affectationFineSiDispo = true)
    {
        $this->affectationFineSiDispo = $affectationFineSiDispo;
        return $this;
    }



    public function isDisplayUserInfo(): bool
    {
        return $this->displayUserInfo;
    }



    public function setDisplayUserInfo(bool $displayUserInfo): UserCurrent
    {
        $this->displayUserInfo = $displayUserInfo;

        return $this;
    }




}
