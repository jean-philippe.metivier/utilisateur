<?php

namespace UnicaenUtilisateur\Form\Strategy;

use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use Laminas\Hydrator\Strategy\StrategyInterface;

class RoleParentStrategy implements StrategyInterface
{
    use RoleServiceAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function extract($value, ?object $object = null)
    {
        if (null != $value) {
            return $value->getId();
        }

        return null;
    }

    /**
     * {@inheritDoc}
     */
    public function hydrate($value, ?array $data)
    {
        if (null != $value) {
            $parent = $this->roleService->find($value);
            return $parent;
        }

        return null;
    }
}
