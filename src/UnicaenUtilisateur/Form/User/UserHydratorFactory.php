<?php

namespace UnicaenUtilisateur\Form\User;

use Psr\Container\ContainerInterface;
use UnicaenUtilisateur\Service\User\UserService;

class UserHydratorFactory {

    /**
     * @param ContainerInterface $container
     * @return UserHydrator
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container) : UserHydrator
    {
        /**
         * @var UserService $userService
         */
        $userService = $container->get(UserService::class);
        
        $hydrator = new UserHydrator();
        $hydrator->setUserService($userService);
        return $hydrator;
    }
}