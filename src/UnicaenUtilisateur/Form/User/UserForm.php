<?php 

namespace UnicaenUtilisateur\Form\User;

use DoctrineModule\Validator\NoObjectExists;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;
use Laminas\Form\Element\Button;
use Laminas\Form\Element\Email;
use Laminas\Form\Element\Password;
use Laminas\Form\Element\Text;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator\Identical;
use Laminas\Validator\NotEmpty;
use Laminas\Validator\Regex;

class UserForm extends Form
{
    use EntityManagerAwareTrait;
    use UserServiceAwareTrait;

    public function init()
    {
        $this->setAttribute('id', 'form-user');

        // libelle
        $this->add([
            'type' => Text::class,
            'name' => 'prenom',
            'options' => [
                'label' => "Prénom :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'prenom',
            ],
        ]);

        //nom afficher
        $this->add([
            'type' => Text::class,
            'name' => 'nom',
            'options' => [
                'label' => "Nom :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'nom',
            ],
        ]);

        //email
        $this->add([
            'type' => Email::class,
            'name' => 'email',
            'options' => [
                'label' => "Adresse électronique :",
                'label_attributes' => [
                    'class' => 'required',
                ],
            ],
            'attributes' => [
                'id' => 'email',
            ],
        ]);

//        //password
//        $this->add([
//            'type' => Password::class,
//            'name' => 'password',
//            'options' => [
//                'label' => "Mot de passe :",
//                'label_attributes' => [
//                    'class' => 'required',
//                ],
//            ],
//            'attributes' => [
//                'id' => 'password',
//            ],
//        ]);
//
//        //password2
//        $this->add([
//            'type' => Password::class,
//            'name' => 'password2',
//            'options' => [
//                'label' => "Vérification du mot de passe :",
//                'label_attributes' => [
//                    'class' => 'required',
//                ],
//            ],
//            'attributes' => [
//                'id' => 'password2',
//            ],
//        ]);

        //submit
        $this->add([
            'type' => Button::class,
            'name' => 'enregistrer',
            'options' => [
                'label' => '<i class="fas fa-save"></i> Enregistrer',
                'label_options' => [
                    'disable_html_escape' => true,
                ],
            ],
            'attributes' => [
                'type' => 'submit',
                'class' => 'btn btn-primary',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'nom'      => [
                'required' => true,
            ],
            'prenom'      => [
               'required' => true,
            ],
            'email'         => [
                'required' => true,
                'validators' => [
                    [
                        'name' => NotEmpty::class,
                        'options' => [
                            'messages' => [
                                NotEmpty::IS_EMPTY => "Veuillez renseigner un email."
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => NoObjectExists::class,
                        'options' => [
                            'object_manager' => $this->getEntityManager(),
                            'object_repository'   => $this->userService->getRepo(),
                            'fields'   => 'email',
                            'messages' => [
                                NoObjectExists::ERROR_OBJECT_FOUND => "L'email est déjà utilisé dans l'application.",
                            ],
                        ],
                    ]
                ],
            ],

//            'password'     => [
//                'required' => true,
//                'validators' => [
//                    [
//                        'name' => NotEmpty::class,
//                        'options' => [
//                            'messages' => [
//                                NotEmpty::IS_EMPTY => "Veuillez renseigner un mot de passe."
//                            ],
//                            'break_chain_on_failure' => true,
//                        ],
//                    ],
//                    [
//                        'name' => Regex::class,
//                        'options' => [
//                            'pattern' => '/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*_])(?=.{8,})/',
//                            'messages' => [
//                                Regex::NOT_MATCH => "Le mot de passe choisi est trop simple. <br/> <b><u>N.B.:</u></b> Le mot de passe doit avoir une longueur minimale de 8 caractères avec au moins : <ul> <li>une majuscule ;</li> <li>une minuscule ;</li> <li>un chiffre ;</li> <li>un caractère spécial.</li></ul>",
//                            ],
//                            'break_chain_on_failure' => true,
//                        ],
//                    ],
//                ],
//            ],
//
//            'password2'     => [
//                'required' => true,
//                'validators' => [
//                    [
//                        'name' => NotEmpty::class,
//                        'options' => [
//                            'messages' => [
//                                NotEmpty::IS_EMPTY => "Veuillez renseigner la confirmation du mot de passe."
//                            ],
//                            'break_chain_on_failure' => true,
//                        ],
//                    ],
//                    new Identical('password'),
//                ],
//            ],
        ]));
    }
}