<?php

namespace UnicaenUtilisateur\Form\User;

use Laminas\Hydrator\HydratorInterface;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

class UserHydrator implements HydratorInterface {

    use UserServiceAwareTrait;
    
    /**
     * NEVER USED
     * @param User $object
     * @return array|mixed[]
     */
    public function extract(object $object): array
    {
        $data = [
            'prenom' => $object->getDisplayName(),
            'nom' => $object->getDisplayName(),
            'email' => $object->getEmail(),
        ];
        return $data;
    }

    /**
     * @param array $data
     * @param User $object
     * @return User
     */
    public function hydrate(array $data, object $object)
    {
        $prenom = (isset($data['prenom']) AND trim($data['prenom']) !== "")?trim($data['prenom']):null;
        $nom = (isset($data['nom']) AND trim($data['nom']) !== "")?trim($data['nom']):null;
        $email = (isset($data['email']) AND trim($data['email']) !== "")?trim($data['email']):null;
        
        $username = strtolower($nom) . ($this->userService->getMaxId() + 1) ;
        $object->setUsername($username);
        $object->setDisplayName($prenom." ".$nom);
        $object->setEmail($email);
        return $object;
        
    }


}