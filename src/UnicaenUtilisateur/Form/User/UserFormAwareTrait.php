<?php

namespace UnicaenUtilisateur\Form\User;

trait UserFormAwareTrait
{
    /**
     * @var UserForm
     */
    protected $userForm;

    /**
     * @param UserForm $userRechercheForm
     * @return UserForm
     */
    public function setUserForm(UserForm $userRechercheForm) : UserForm
    {
        $this->userForm = $userRechercheForm;

        return $this->userForm;
    }

    /**
     * @return UserForm
     */
    public function getUserForm() : UserForm
    {
        return $this->userForm;
    }
}