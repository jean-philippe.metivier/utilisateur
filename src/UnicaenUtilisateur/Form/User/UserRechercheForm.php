<?php

namespace UnicaenUtilisateur\Form\User;

use  Application\Entity\Db\GroupeType;
use DoctrineModule\Form\Element\ObjectSelect as DoctrineObjectSelect;
use UnicaenApp\Form\Element\SearchAndSelect;
use Laminas\Form\Element;
use Laminas\Filter;
use Laminas\Form\Form;
use Laminas\InputFilter\Factory;
use Laminas\Validator;

class UserRechercheForm extends Form
{
    public function init()
    {
        $this->setAttribute('method', 'post');

        $this->add([
            'type' => Element\Hidden::class,
            'name' => 'utilisateurId',
            'attributes' => [
                'id' => 'utilisateur-id'
            ]
        ]);

        $this->add([
            'type' => Element\Text::class,
            'name' => 'utilisateur',
            'attributes' => [
                'id' => 'utilisateur',
                'placeholder' => "Nom ou identifiant de connexion..."
            ],
        ]);

        $this->add([
            'type' => Element\Button::class,
            'name' => 'selectionner',
            'options' => [
                'label' => 'Sélectionner',
            ],
            'attributes' => [
                'id' => 'selectionner',
                'type' => 'submit',
                'class' => 'btn btn-primary',
                'style' => 'display: none',
            ],
        ]);

        $this->setInputFilter((new Factory())->createInputFilter([
            'utilisateur' => [
                'required' => true,
                'filters' => [
                    ['name' => Filter\StripTags::class],
                    ['name' => Filter\StringTrim::class],
                ],
                'validators' => [
                    [
                        'name' => Validator\NotEmpty::class,
                        'options' => [
                            'messages' => [
                                Validator\NotEmpty::IS_EMPTY => "Veuillez saisir les premiers caractères de votre recherche."
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                    [
                        'name' => Validator\Callback::class,
                        'options' => [
                            'callback' => function ($value, $context) {
                                return null != $context['utilisateurId'];
                            },
                            'messages' => [
                                Validator\Callback::INVALID_VALUE => "Vous devez rechercher puis sélectionner un élément dans la liste proposée.",
                            ],
                            'break_chain_on_failure' => true,
                        ],
                    ],
                ],
            ],

        ]));
    }
}