<?php

namespace UnicaenUtilisateur\ORM\Event\Listeners;

use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;
use Laminas\Authentication\AuthenticationService;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Service\User\UserService;

class HistoriqueListenerFactory implements FactoryInterface
{
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /**
         * @var AuthenticationService $authenticationService
         * @var UserService $userService
         */
        $authenticationService = $container->get('Laminas\Authentication\AuthenticationService');
        
        /** @var UserService $userService */
        $config = $container->get('Configuration')['unicaen-utilisateur'];
        $userId = isset($config['default-user'])?$config['default-user']:null;
//        $user = isset($config['default-user'])?($em->find($config['default-user'])):null;
        
        $listener = new HistoriqueListener();
        $listener->setAuthenticationService($authenticationService);
        $listener->setDefaultUser($userId);
//        $listener->setDefaultUser($user);

        return $listener;
    }
}
