<?php

namespace UnicaenUtilisateur\Controller;

use Interop\Container\ContainerInterface;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use UnicaenUtilisateur\Form\User\InitCompteForm;
use UnicaenUtilisateur\Form\User\UserForm;
use UnicaenUtilisateur\Form\User\UserRechercheForm;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuServiceInterface;
use UnicaenUtilisateur\Service\Role\RoleService;
use UnicaenUtilisateur\Service\User\UserService;

class UtilisateurControllerFactory
{

    /**
     * @param ContainerInterface $container
     * @param string $requestedName
     * @param array|null $options
     * @return UtilisateurController|object
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function __invoke(ContainerInterface $container, string $requestedName, array $options = null)
    {
        /**
         * @var RoleService $roleService
         * @var UserService $userService
         */
        $config = $container->get('Config');
        $moduleConfig = $config['unicaen-utilisateur'];
        $usernamefield = $config['unicaen-auth']['local']['ldap']['username'];
        $roleService = $container->get(RoleService::class);
        $userService = $container->get(UserService::class);
        $initCompte = $container->get('FormElementManager')->get(InitCompteForm::class);
        $userForm = $container->get('FormElementManager')->get(UserForm::class);
        $userRechercheForm = $container->get('FormElementManager')->get(UserRechercheForm::class);

        // services de recherche d'un individu : application, ldap, octopus,...
        $rechercheServices = [];
        if(isset($moduleConfig['recherche-individu']) && !empty($moduleConfig['recherche-individu'])) {
            foreach ($moduleConfig['recherche-individu'] as $key => $service) {
                $service = $container->get($service);
                if($service instanceof RechercheIndividuServiceInterface) {
                    $rechercheServices[$key] = $service;
                }
            }
        }

        $identityProvider = [];
        if (isset($moduleConfig['identity-provider']) && !empty($moduleConfig['identity-provider'])) {
            foreach ($moduleConfig['identity-provider'] as $value)
            $identityProvider[]= $container->get($value);
        }

        $controller = new UtilisateurController();
        $controller->setRoleService($roleService);
        $controller->setUserService($userService);
        $controller->setSearchServices($rechercheServices);
        $controller->setIdentityProvider($identityProvider);
        $controller->setInitCompteForm($initCompte);
        $controller->setUserForm($userForm);
        $controller->setUserRechercheForm($userRechercheForm);

        $controller->setUsernameField($usernamefield);

        return $controller;
    }
}