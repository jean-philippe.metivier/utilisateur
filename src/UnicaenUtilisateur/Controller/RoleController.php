<?php

namespace UnicaenUtilisateur\Controller;

use BjyAuthorize\Provider\Identity\ProviderInterface;
use DateTime;
use Exception;
use UnicaenApp\View\Model\CsvModel;
use UnicaenUtilisateur\Entity\Db\RoleInterface;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenUtilisateur\Form\Role\RoleFormAwareTrait;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\ViewModel;

/** @method FlashMessenger flashMessenger() */

class RoleController extends AbstractActionController {
    use RoleServiceAwareTrait;
    use RoleFormAwareTrait;

    /** @var ProviderInterface */
    private $identityProviders;

    public function setIdentityProviders($provider)
    {
        $this->identityProviders = $provider;
    }

    public function indexAction() : ViewModel
    {
        /** @var RoleInterface[] $roles */
        $roles = $this->roleService->findAll();

        return new ViewModel([
            'roles' => $roles,
            'identityproviders' => $this->identityProviders,
        ]);
    }

    public function exporterAction() : CsvModel
    {
        $role = $this->roleService->getRequestedRole($this);

        /** @var UserInterface[] $users */
        $users = [];
        if (!$role->isAuto()) {
            $users = $role->getUsers()->toArray();
            usort($users, function (UserInterface $a, UserInterface $b) {
                return $a->getDisplayName() > $b->getDisplayName();
            });
        } else {
            if ($this->identityProviders !== null) {
                foreach ($this->identityProviders as $identityProvider) {
                    if (method_exists($identityProvider, "computeUsersAutomatiques")) {
                        $users = array_merge($users, $identityProvider->computeUsersAutomatiques($role->getRoleId()));
                    }
                }
            }
        }


        $headers = [ 'Identifiant de connexion', 'Nom', 'Adresse email'];

        $records = [];
        foreach ($users as $user) {
            $item  = [];
            $item[]  = $user->getUsername();
            $item[]  = $user->getDisplayName();
            $item[]  = $user->getEmail();
            $records[] = $item;
        }

        $date = (new DateTime())->format('Ymd-His');
        $filename="export_utilisateur_".str_replace(' ','-',$role->getRoleId())."_".$date.".csv";
        $CSV = new CsvModel();
        $CSV->setDelimiter(';');
        $CSV->setEnclosure('"');
        $CSV->setHeader($headers);
        $CSV->setData($records);
        $CSV->setFilename($filename);

        return $CSV;
    }

    public function ajouterAction() : ViewModel
    {
        $role = $this->roleService->getEntityInstance();
        $form = $this->roleForm;
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-role/ajouter', [], [], true));
        $form->bind($role);

        if ($data = $this->params()->fromPost()) {
            $form->setData($data);
            if ($form->isValid()) {
                try {
                    $this->roleService->create($role);
                    $this->flashMessenger()->addSuccessMessage(sprintf("Rôle <strong>%s</strong> créé avec succès.", $role->getLibelle()));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la création du rôle <strong>%s</strong>.", $role->getLibelle()));
                }
            }
        }

        $view = new ViewModel();
        $view->setTemplate('unicaen-utilisateur/role/template/form-role');
        $view->setVariables([
            'title' => 'Ajout d\'un nouveau rôle',
            'form' => $form,
        ]);

        return $view;
    }

    public function modifierAction() : ViewModel
    {
        $role = $this->roleService->getRequestedRole($this);
        $form = $this->roleForm;
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-role/modifier', ['role' => $role->getId()], [], true));
        $form->bind($role);

        if ($data = $this->params()->fromPost()) {
            $form->setData($data);
            if ($form->isValid()) {
                try {
                    $this->roleService->update($role);
                    $this->flashMessenger()->addSuccessMessage(sprintf("Rôle <strong>%s</strong> modifié avec succès.", $role->getLibelle()));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la modification du rôle <strong>%s</strong>.", $role->getLibelle()));
                }
            }
        }

        $view = new ViewModel();
        $view->setTemplate('unicaen-utilisateur/role/template/form-role');
        $view->setVariables([
            'title' => "Modification du rôle",
            'form' => $form,
        ]);

        return $view;
    }

    public function supprimerAction() : array
    {
        $role = null;
        try {
            $role = $this->roleService->getRequestedRole($this);
            $this->roleService->delete($role);
            $this->flashMessenger()->addSuccessMessage(sprintf("Rôle <strong>%s</strong> supprimé avec succès.", $role->getLibelle()));
        } catch (Exception $e) {
            $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la suppression du rôle <strong>%s</strong>.", $role->getLibelle()));
        }

        return compact('role');
    }
}