<?php

namespace UnicaenUtilisateur\Controller;

use BjyAuthorize\Provider\Identity\ProviderInterface;
use DateTime;
use Exception;
use UnicaenApp\Service\EntityManagerAwareTrait;
use UnicaenApp\View\Model\CsvModel;
use UnicaenUtilisateur\Entity\Db\UserInterface;
use UnicaenUtilisateur\Form\User\InitCompteFormAwareTrait;
use UnicaenUtilisateur\Form\User\UserFormAwareTrait;
use UnicaenUtilisateur\Form\User\UserRechercheFormAwareTrait;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuResultatInterface;
use UnicaenUtilisateur\Service\RechercheIndividu\RechercheIndividuServiceInterface;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\Mvc\Plugin\FlashMessenger\FlashMessenger;
use Laminas\View\Model\JsonModel;
use Laminas\View\Model\ViewModel;

/** @method FlashMessenger flashMessenger() */

class UtilisateurController extends AbstractActionController
{
    use RoleServiceAwareTrait;
    use UserServiceAwareTrait;
    use EntityManagerAwareTrait;
    use InitCompteFormAwareTrait;
    use UserFormAwareTrait;
    use UserRechercheFormAwareTrait;


    /**
     * @var RechercheIndividuServiceInterface[]
     */
    public $searchServices;

    /**
     * @var ProviderInterface
     */
    private $identityProvider;

    /**
     * @param $searchServices
     */
    public function setSearchServices($searchServices) : void
    {
        $this->searchServices = $searchServices;
    }

    /**
     * @param $provider
     */
    public function setIdentityProvider($provider) : void
    {
        $this->identityProvider = $provider;
    }

    private ?string $usernamefield;
    public function setUsernamefield(?string $usernamefield): void
    {
        $this->usernamefield = $usernamefield;
    }

    public function indexAction()
    {
        $roles = $this->roleService->findAll();
        $form = $this->userRechercheForm;

        if ($data = $this->params()->fromPost()) {
            $form->setData($data);
            if ($form->isValid()) {
                list($source, $id) = explode('||', $data['utilisateurId']);

                switch ($source) {
                    case 'app' :
                        return $this->redirect()->toRoute('unicaen-utilisateur', [], ["query" => ["id" => $id]], true);

                    default :
                        if (isset($this->searchServices[$source])) {
                            $sourceUser = $this->searchServices[$source]->findById($id);
                            $newUser = $this->userService->importFromRechercheIndividuResultatInterface($sourceUser, $source);
                            $user = $this->userService->findByUsername($newUser->getUsername($this->usernamefield));

                            if (!$user) {
                                $user = $this->userService->create($newUser);
                            } else {
                                $this->flashMessenger()->addErrorMessage(
                                    sprintf("L'utilisateur <strong>%s (%s)</strong> existe déjà dans l'application.", $user->getDisplayName(), $user->getUsername($this->usernamefield))
                                );
                            }

                            $this->flashMessenger()->addSuccessMessage(sprintf("Utilisateur <strong>%s</strong> créé avec succès.", $user->getDisplayName()));
                            return $this->redirect()->toRoute('utilisateur', [], ["query" => ["id" => $user->getId()]], true);
                        }
                        else {
                            $this->flashMessenger()->addErrorMessage("La source \"$source\" n'est pas déclarée dans la configuration.");

                            return $this->redirect()->toRoute('utilisateur', [], [], true);
                        }
                }
            }
        }

        $utilisateurId = $this->params()->fromQuery("id");
        $utilisateur = null;
        $rolesAttributes = [];
        $rolesDisponibles = [];
        if ($utilisateurId !== null AND $utilisateurId !== "") {
            $utilisateur = $this->userService->find($utilisateurId);
            if($utilisateur instanceof UserInterface) {
                $rolesAttributes = $utilisateur->getRoles()->toArray();
                $rolesDisponibles = array_diff($roles, $rolesAttributes);
            }
        }

        $rolesAutomatiques = [];
        foreach ($this->identityProvider as $identityProvider) {
            $rolesAutomatiques = array_merge($rolesAutomatiques, $identityProvider->computeRolesAutomatiques($utilisateur));
        }

        // TODO recupérer le provider qui va bien ...
        return new ViewModel([
            'title' => "Gestion des utilisateurs",
            'form' => $form,
            'utilisateur' => $utilisateur,
            'roles' => $roles,
            'rolesAttributes' => $rolesAttributes,
            'rolesDisponibles' => $rolesDisponibles,
            'rolesAutomatiques' => $rolesAutomatiques,
        ]);
    }

    public function ajouterAction()
    {
        $utilisateur = $this->userService->getEntityInstance();
        $form = $this->getUserForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-utilisateur/ajouter', [], [], true));
        $form->bind($utilisateur);

        if ($data = $this->params()->fromPost()) {
            $form->setData($data);
            if ($form->isValid()) {
                try {
                    $this->userService->createLocal($utilisateur);
                    $this->flashMessenger()->addSuccessMessage(sprintf("Utilisateur <strong>%s</strong> créé avec succès. Un courrier életronique vient de vous être envoyé pour l'initialisation de votre mot de passe", $utilisateur->getDisplayName()));
                } catch (Exception $e) {
                    $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la création de l'utilisateur <strong>%s</strong>.", $utilisateur->getDisplayName()));
                }

                return $this->redirect()->toRoute('unicaen-utilisateur', [], ["query" => ["id" => $utilisateur->getId()]], true);
            }
        }

        $title = "Création d'un utilisateur local";
        return compact('title', 'form');
    }

    public function changerMotDePasseAction()
    {
        $utilisateur = $this->userService->getRequestedUser($this, 'user');
        $token = $this->params()->fromRoute('token');
        $reset = $utilisateur->getPasswordResetToken();

        $form = $this->getInitCompteForm();
        $form->setAttribute('action', $this->url()->fromRoute('unicaen-utilisateur/changer-mot-de-passe', ['user' => $utilisateur->getId(), 'token' => $utilisateur->getPasswordResetToken()], ['force_canonical' => true], true));
        
        $request = $this->getRequest();
        if ($request->isPost()) {
            $data = $request->getPost();
            $this->userService->changerPassword($utilisateur,$data);
            $this->flashMessenger()->addSuccessMessage("Mot de passe changé avec succés");
            return $this->redirect()->toRoute('home', [], [], true);
        }

        return new ViewModel([
            'utilisateur' => $utilisateur,
            'token' => $token,
            'form' => $form,

        ]);
                
    }

    public function addRoleAction()
    {
        $utilisateur = $this->userService->getRequestedUser($this);
        $roleId = $this->params()->fromRoute("role");
        $role = $this->roleService->find($roleId);

        if ($utilisateur !== null && $role !== null) {
            $this->userService->addRole($utilisateur, $role);
            $result = true;
        }

        return new JsonModel([
            "result" => $result ?: false,
            "message" => $result
                ? sprintf("Ajout du rôle <strong>%s</strong> à l'utilisateur <strong>%s</strong>.", $role->getLibelle(), $utilisateur->getDisplayName())
                : sprintf("Erreur lors de l'ajout du rôle <strong>%s</strong> à l'utilisateur <strong>%s</strong>.", $role->getLibelle(), $utilisateur->getDisplayName())
        ]);
    }

    public function removeRoleAction()
    {
        $utilisateur = $this->userService->getRequestedUser($this);
        $roleId = $this->params()->fromRoute("role");
        $role = $this->roleService->find($roleId);

        if ($utilisateur !== null && $role !== null) {
            $this->userService->removeRole($utilisateur, $role);
            $result = true;
        }

        return new JsonModel([
            "result" => $result ?: false,
            "message" => $result
                ? sprintf("Suppression du rôle <strong>%s</strong> à l'utilisateur <strong>%s</strong>.", $role->getLibelle(), $utilisateur->getDisplayName())
                : sprintf("Erreur lors de la suppression du rôle <strong>%s</strong> à l'utilisateur <strong>%s</strong>.", $role->getLibelle(), $utilisateur->getDisplayName())
        ]);
    }

    public function changerStatutAction()
    {
        $utilisateur = null;
        try {
            $utilisateur = $this->userService->getRequestedUser($this);
            $utilisateur = $this->userService->changeStatus($utilisateur);

            $this->flashMessenger()->addSuccessMessage(sprintf("Utilisateur <strong>%s</strong> %s avec succès.",
                $utilisateur->getDisplayName(),
                $utilisateur->getState() ? 'activé' : 'suspendu'));
        } catch (Exception $e) {
            $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de %s de l'utilisateur <strong>%s</strong>.",
                $utilisateur->getDisplayName(),
                $utilisateur->getState() ? "l'activation" : 'la suspension'));
        }

        return compact('utilisateur');
    }

    public function supprimerAction()
    {
        $utilisateur = null;
        try {
            $utilisateur = $this->userService->getRequestedUser($this);
            $this->userService->delete($utilisateur);
            $this->flashMessenger()->addSuccessMessage(sprintf("Utilisateur <strong>%s</strong> supprimé avec succès.", $utilisateur->getDisplayName()));
        } catch (Exception $e) {
            $this->flashMessenger()->addErrorMessage(sprintf("Une erreur est survenu lors de la suppression de l'utilisateur <strong>%s</strong>.", $utilisateur->getDisplayName()));
        }

        return compact('utilisateur');
    }

    public function rechercherAction() : JsonModel 
    {
        $term = $this->params()->fromQuery('term');

        // on récupère d'abord les utilisateurs enregistrés dans l'application
        $appService = $this->searchServices['app'];
        $currentUsers = $appService->findByTerm($term);
        $finalUsers['app'] = $currentUsers;
        unset($this->searchServices['app']);

        /** @var RechercheIndividuServiceInterface $service */
        foreach ($this->searchServices as $key => $service) {
            $currentUsernames = array_map(function($u) {
                /** @var RechercheIndividuResultatInterface $u */
                return $u->getUsername($this->usernamefield);
            }, $currentUsers);
            $users = $service->findByTerm($term);
            $usersFiltered = array_filter($users, function($e) use ($currentUsernames) {
                /** @var RechercheIndividuResultatInterface $e  */
                return !in_array($e->getUsername($this->usernamefield), $currentUsernames);
            });
            $currentUsers = array_merge($currentUsers, $usersFiltered);
            $finalUsers[$key] = $usersFiltered;
        }

        $result = [];
        foreach ($finalUsers as $key => $individus) {
            foreach ($individus as $individu) {
                $result[] = [
                    'id' => $key . '||' . $individu->getId(),
                    'label' => $individu->getDisplayName(),
                    'extra' => sprintf(' <span class="text-highlight">/</span> %s <span class="text-highlight">/</span> %s %s',
                        $individu->getUsername($this->usernamefield),
                        $individu->getEmail() ?: "aucun email",
                        ($key === 'app') ? '<i title="Utilisateur existant" class="fas fa-check-circle text-success"></i>' : '')
                ];
            }
        }

        usort($result, function ($a, $b) { return $a['label'] > $b['label']; });

        return new JsonModel($result);
    }

    public function listerAction() : ViewModel
    {
        $users = $this->userService->findAll();

        return new ViewModel([
            'title' => "Gestion des utilisateurs",
            'users' => $users,
        ]);
    }
    
    public function exporterAction() : CsvModel
    {
        $users = $this->userService->findAll();
        $headers = [ 'Identifiant de connexion', 'Nom', 'Activé', 'Rôle'];

        $records = [];
        foreach ($users as $user) {
            $item  = [];
            $item[]  = $user->getUsername($this->usernamefield);
            $item[]  = $user->getDisplayName();
            $item[]  = $user->getState();

            $role_array = [];
            foreach ($user->getRoles() as $role) {
                $role_array[] = $role->getLibelle();
            }
            $item[] = implode("\n", $role_array);
            $records[] = $item;
        }

        $date = (new DateTime())->format('Ymd-His');
        $filename="export_utilisateur_".$date.".csv";
        $CSV = new CsvModel();
        $CSV->setDelimiter(';');
        $CSV->setEnclosure('"');
        $CSV->setHeader($headers);
        $CSV->setData($records);
        $CSV->setFilename($filename);

        return $CSV;    
    }
}