<?php

namespace UnicaenUtilisateur\Provider\Identity;

use BjyAuthorize\Provider\Identity\ProviderInterface;
use UnicaenAuthentification\Provider\Identity\ChainableProvider;
use UnicaenAuthentification\Provider\Identity\ChainEvent;

use UnicaenUtilisateur\Entity\Db\RoleInterface;
use UnicaenUtilisateur\Entity\Db\User;
use UnicaenUtilisateur\Service\Role\RoleServiceAwareTrait;
use UnicaenUtilisateur\Service\User\UserServiceAwareTrait;

abstract class AbstractIdentityProvider implements ProviderInterface, ChainableProvider
{
    use RoleServiceAwareTrait;
    use UserServiceAwareTrait;

    /**
     * @param string $code
     * @return User[]|null
     */
    abstract public function computeUsersAutomatiques(string $code) : ?array ;

    /**
     * @param User|null $user
     * @return string[]|RoleInterface[]
     */
    abstract public function computeRolesAutomatiques(?User $user = null) : array ;

    /**
     * @return string[]|RoleInterface[]
     */
    public function getIdentityRoles() : array
    {
        return $this->computeRolesAutomatiques();
    }

    /**
     * @param ChainEvent $e
     */
    public function injectIdentityRoles(ChainEvent $e) {
        $e->addRoles($this->getIdentityRoles());
    }
}