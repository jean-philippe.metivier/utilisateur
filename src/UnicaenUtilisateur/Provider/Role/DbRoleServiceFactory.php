<?php

namespace UnicaenUtilisateur\Provider\Role;

use Interop\Container\ContainerInterface;
use UnicaenUtilisateur\Service\Role\RoleService;
use Laminas\ServiceManager\FactoryInterface;
use Laminas\ServiceManager\ServiceLocatorInterface;

/**
 * Factory responsible of instantiating {@see \UnicaenUtilisateur\Provider\Role\DbRole}
 */
class DbRoleServiceFactory implements FactoryInterface
{
    /**
     * {@inheritDoc}
     */
    public function createService(ServiceLocatorInterface $serviceLocator)
    {
        return $this->__invoke($serviceLocator, '?');
    }

    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        /* @var $serviceRole RoleService */
        $serviceRole = $container->get(RoleService::class);

        return new DbRole($serviceRole->getRepo());
    }
}
